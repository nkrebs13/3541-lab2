﻿using UnityEngine;
using System.Collections;

public class MeshCharacter : MonoBehaviour {
    float width = 0.75f;
    float height = 0.75f;


    // Doesn't create, somethign wrong with mesh filter (can't find it or something)
    
	// Use this for initialization
	void Start () {
        //creating vertices
        MeshFilter mf = GetComponent<MeshFilter>();
        Mesh mesh = new Mesh();
        mf.mesh = mesh;
        
        //MeshRenderer rend = GetComponent<MeshRenderer>();
        

        //mf.mesh = mesh;
        Vector3[] vertices = new Vector3[4];
        vertices[0] = new Vector3(0.0f, 0.0f, 0.0f);
        vertices[1] = new Vector3(width, 0.0f, 0.0f);
        vertices[2] = new Vector3(0.0f, height, 0.0f);
        vertices[3] = new Vector3(width, height, 0.0f);
        mesh.vertices = vertices;

        //make triangles
        int[] triangle = new int[6];
        triangle[0] = 0;
        triangle[1] = 2;
        triangle[2] = 1;

        triangle[3] = 2;
        triangle[4] = 3;
        triangle[5] = 1;
        mesh.triangles = triangle;


        //GetComponent<MeshFilter>().mesh = mesh;

        //Normals
        Vector3[] normals = new Vector3[4];
        normals[0] = -Vector3.forward;
        normals[1] = -Vector3.forward;
        normals[2] = -Vector3.forward;
        normals[3] = -Vector3.forward;
        mesh.normals = normals;

        //UV
        Vector2[] uv = new Vector2[4];
        uv[0] = new Vector2(0,0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2 (0, 1);
        uv[3] = new Vector2(1, 1);
        mesh.uv = uv;

        GetComponent<Renderer>().material.color = Color.black;

    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
