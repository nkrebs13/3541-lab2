﻿using UnityEngine;
using System.Collections;

public class CreateMaze : MonoBehaviour {
    float[,] maze = new float[20,20];
    GameObject cube;
    // Use this for initialization
    void Start () {
        int j = 0; int k = 0;
        //Create Intial grid of cubes
        // foreach (j,k) in maze;
        //      if(maze[if,j]) {

        // }


        //cube.transform.parent = maze.transform;
       // cube =GameObject.CreatePrimitive(PrimitiveType.Cube);
       // cube.GetComponent<Renderer>().material.color = Color.red;
        for (j = 0; j < 21; j++)
        {
            for(k = 0; k < 21; k++)
            {
                cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
                cube.transform.position = new Vector3(j, 0, k);
                cube.transform.parent = transform;
                cube.name = "cube" + j.ToString() + "'" + k.ToString();
               // Instantiate(cube, new Vector3(j, 0, k), Quaternion.identity);
                cube.GetComponent<Renderer>().material.color = Color.red;
                //Set initial path through maze, couldn't use cubes outside of loop so have to use long if statement
                // each condition is basically a line that draws the way out of the maze
                if ((j == 3 && k < 3) || (j >= 4 && j <= 7 && k == 2) || (k > 2 && k < 6 && j == 7) || (k == 6 && j <= 7 && j >= 5) || (k >= 6 && k < 9 && j == 5) || (k == 8 && j >= 5 && j <= 10) || (k >= 8 && k <= 13 && j == 10))
                {
                    Destroy(cube);
                }
                else
                {
                    //
                }
                // make another if loop so its readable without scrolling
                if ((j >= 7 && j <= 10 && k == 13) || (j == 7 && k <= 13 && k >= 11) || (j <= 7 && j >= 3 && k == 11) || (j == 3 && k >= 11 && k <= 17) || (j >= 3 && j <= 13 && k == 17) || (j == 17 && k <= 20 && k >= 14))
                {
                    Destroy(cube);
                }
                //end of path and some false trails
                if ((j == 10 && k <= 16 && k >= 15) || (k == 15 && j >= 11 && j <= 14) || (j == 14 && k <= 14 && k >= 13) || (k == 13 && j >= 15 && j <= 17) || (j==2 && k==7))
                {
                    Destroy(cube);
                }
                //more false trails
                if((k==3 && j>=8 && j<=12) || (j==12 && k>=1 && k<=6) || (k==6 && j<=12 && j>=9) || (j==10 && k==5) || (k==1 && j<=12 && j>=9) || (k==13 && j>=11 && j<=12) || (j==12 && k>=9 && k<=13))
                {
                    Destroy(cube);
                }
                if((j==5 && k<=16 && k>=13) || (k==15 && j>=6 && j<=8) || (j==8 && k==11)|| (k==4 && j<=6 && j>=1) || (j==3 && k>=4 && k<=7) || (k==9 && j>=13 && j<=15) || (j==14 && k<=8 && k>=5) || (j==1 && k>=5 && k<=11) )
                {
                    Destroy(cube);
                }
                if((j>=2 && j<=3 && k==9) || (j<=2 && j>=1 && k==1) || (j==1 && k==2) || (j==5 && k==1) || (j>=1 && j<=2 && k==17) || (j>=15 && j<=17 && k==5) || (j==16 && k<=4 && k>=3) || (j==16 && k>=9 && k<=11))
                {
                    Destroy(cube);
                }
                if((j<=15 && j>=14 && k==11) || (k==3 & j<=15 && j>=14) || (j==14 && k<=3 && k>=1) || (k==1 && j>=15 && j<=19) || (j==19 && k>=2 && k<=9) || (k==7 && j<=18 && j>=16) || (k==11 && j>=17 && j<=19) || (j==19 && k>=12 && k<=19))
                {
                    Destroy(cube);
                }
                if((j==18 && k==9) || (j==1 && k<=19 && k>=13) || (k==19 && ((j>=2 && j<=5) || (j>=12 && j<=15))) || ((j==8 || j==12) && k>=18 && k<=19) || (k==19 && j<=10 && j>=7) || (j==15 && k>=17 && k<=19) || (j==16 && k==15))
                {
                    Destroy(cube);
                }
            }

        }



    }

	// Update is called once per frame
	void Update () {
    }
}
