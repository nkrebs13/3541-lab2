﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

    bool isRotating;
    // Use this for initialization
    void Start () {
        isRotating = false;
    }

	// Update is called once per frame
	void Update () {


        //This works, but uses 2 diff buttons
        //making mesh object child of pivotPoint will make it rotate with maze


        if(Input.GetKey(KeyCode.Alpha0))
        {
            isRotating = true;
        }

        if(isRotating)
        {
            transform.Rotate(0, 100f *Time.deltaTime, 0);
            isRotating = false;
        }

    }


}
